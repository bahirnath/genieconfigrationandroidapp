package gsmartadmin.genieiot.com.myapplication;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import gsmartadmin.genieiot.com.myapplication.Adapter.ExpandableListAdapter;
import gsmartadmin.genieiot.com.myapplication.Database.DatabaseHandler;
import gsmartadmin.genieiot.com.myapplication.Hotspot.ILongPressListener;

/**
 * Created by root on 12/1/16.
 */

public class RoomListAcitivity extends AppCompatActivity   {

    private ExpandableListView expListView;
    private ExpandableListAdapter listAdapter;
    private ArrayList<String> listDataHeader,keyList;
    private HashMap<String, List<String>> listDataChild;
    DatabaseHandler db ;
    String ID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_list_activity);
        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        db=new DatabaseHandler(this);
        // preparing list data

        prepareListData();
        setAdapter();
        expListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id)
            {

                final CharSequence[] strArray = {"Delete"};
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RoomListAcitivity.this);
                alertDialogBuilder.setItems(strArray,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        alertDialogDelete(keyList.get(position));
                                        break;
//
                                   // case 1:
                                       // Intent intent=new Intent(RoomListAcitivity.this,EditPanel.class);
//                                        HashMap<String,String> map=db.getHomeInfo(keyList.get(position));
//                                        Log.d("Map Data",""+map);
//                                        intent.putExtra("CONFIGURATION_DETAILS",map);
//                                        startActivity(intent);
//                                        break;
                                }
                            }
                        });
                alertDialogBuilder.show();
                return false;
            }
        });
    
    }

    private void setAdapter() {

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild,keyList);
        // setting list adapter
        expListView.setAdapter(listAdapter);
    }

    private void alertDialogDelete(final String key)  {
        final Dialog dialog = new Dialog(RoomListAcitivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_delete);

        TextView txtCancel= (TextView) dialog.findViewById(R.id.txtCancel);
        TextView txtDelete= (TextView) dialog.findViewById(R.id.txtDelete);

        txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                db.deletePanel(key);
                prepareListData();
                setAdapter();

                Toast.makeText(RoomListAcitivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                dialog.dismiss();


            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        keyList = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        ArrayList<HashMap<String,String>> map=db.getAllHomeInfo();

        for(int i=0;i<map.size();i++){

            List<String> list = new ArrayList<String>();
            list.add(map.get(i).get(DatabaseHandler.SW1));
            list.add(map.get(i).get(DatabaseHandler.SW2));
            list.add(map.get(i).get(DatabaseHandler.SW3));
            list.add(map.get(i).get(DatabaseHandler.SW4));
            list.add(map.get(i).get(DatabaseHandler.SW5));
            list.add(map.get(i).get(DatabaseHandler.SW6));

            keyList.add(map.get(i).get(DatabaseHandler.KEY_ID));
            listDataHeader.add(map.get(i).get(DatabaseHandler.ROOMNAME));
            listDataChild.put(map.get(i).get(DatabaseHandler.ROOMNAME),list);

        }

        // Adding child data
//        listDataHeader.add("Top 250");
//        listDataHeader.add("Now Showing");
//        listDataHeader.add("Coming Soon..");

        // Adding child data
//        List<String> top250 = new ArrayList<String>();
//        top250.add("The Shawshank Redemption");
//        top250.add("The Godfather");
//        top250.add("The Godfather: Part II");
//        top250.add("Pulp Fiction");
//        top250.add("The Good, the Bad and the Ugly");
//        top250.add("The Dark Knight");
//        top250.add("12 Angry Men");
//
//        List<String> nowShowing = new ArrayList<String>();
//        nowShowing.add("The Conjuring");
//        nowShowing.add("Despicable Me 2");
//        nowShowing.add("Turbo");
//        nowShowing.add("Grown Ups 2");
//        nowShowing.add("Red 2");
//        nowShowing.add("The Wolverine");
//
//        List<String> comingSoon = new ArrayList<String>();
//        comingSoon.add("2 Guns");
//        comingSoon.add("The Smurfs 2");
//        comingSoon.add("The Spectacular Now");
//        comingSoon.add("The Canyons");
//        comingSoon.add("Europa Report");
//
//        listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
//        listDataChild.put(listDataHeader.get(1), nowShowing);
//        listDataChild.put(listDataHeader.get(2), comingSoon);
    }


}
