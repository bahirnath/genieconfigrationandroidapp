package gsmartadmin.genieiot.com.myapplication;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.Arrays;

import gsmartadmin.genieiot.com.myapplication.Database.DatabaseHandler;
import gsmartadmin.genieiot.com.myapplication.Utils.SwitchSessionManager;
import gsmartadmin.genieiot.com.myapplication.Utils.Utils;

/**
 * Created by root on 3/2/17.
 */

public class ReconfigActivity extends AppCompatActivity implements View.OnClickListener{
    Button btnReConfiguration;
    DatabaseHandler db;
    private ProgressDialog progressDialog;
    private static final String TAG = "ReconfigActivity";
    private SwitchSessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reconfig_activity);
        db=new DatabaseHandler(this);
        sessionManager=new SwitchSessionManager(this);
        btnReConfiguration= (Button) findViewById(R.id.btnReConfiguration);
        btnReConfiguration.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnReConfiguration:
                new AsyncGetConfigurationTask().execute();
                break;
        }
    }

    public class AsyncGetConfigurationTask extends AsyncTask<Void,Void,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog= new ProgressDialog(ReconfigActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String response=null;
            Utils req=new Utils();
            try{
                response=req.doGetRequest("http://"+sessionManager.getTopicName()+":8080/GSmart_final_9jan/home/getinstallationinfo");

                Log.d(TAG, "Get All Data "+response);
            }
            catch(Exception e)
            {

            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(progressDialog!=null){
                progressDialog.cancel();
            }

            if(result!=null)
            {
//                String mm="{\"panelDetails\":\"panel1-BED1-11-123456-GTX11,panel2-BED2-12-120056-GTX11,panel3-HAL1-13-123456-GTX11\",\n" +
//                        "\"status\":\"SUCCESS\"\n" +
//                        "}";
                parseJSONResponse(result);
            }
            else
            {
                Toast.makeText(ReconfigActivity.this, "Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void parseJSONResponse(String result) {
        try{
          JSONObject jResult=new JSONObject(result);

          if(jResult.getString("status").equals("SUCCESS"))
          {
             String panelDetails=jResult.getString("panelDetails");
             String[] panelinfo=panelDetails.split(",");
//             String routeSSID=jResult.getString("routerSSID");
//             String routerPass=jResult.getString("routerPassword");
//             String secureSSID=jResult.getString("secureSSID");
//             String securePassword=jResult.getString("securePassword");

             String homeId=Utils.getSharedPValue(ReconfigActivity.this,Utils.HOMEID);
             for(int i=0;i<panelDetails.length();i++)
             {
                 String[] splitpanel=panelinfo[i].split("-");
                 String topic=splitpanel[0];
                 String roomName=splitpanel[1];
                 String switchDetails=splitpanel[1]+"-"+splitpanel[2]+"-"+splitpanel[3]+"-"+splitpanel[4];
                 db.addNewConfigFile(panelinfo[i],homeId,topic,roomName,switchDetails,splitpanel[5],splitpanel[6],splitpanel[7]);
             }
              finish();
              //Toast.makeText(this,"Data Fetch successfully.",Toast.LENGTH_LONG).show();
              Toast.makeText(this, "Please Check Room List", Toast.LENGTH_SHORT).show();

          }
          else
          {
              Toast.makeText(this,"Unable to fetch configure file.",Toast.LENGTH_LONG).show();
          }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
