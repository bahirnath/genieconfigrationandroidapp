package gsmartadmin.genieiot.com.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by root on 13/1/17.
 */

public class StartSession extends AppCompatActivity implements View.OnClickListener {
    private Button btnStartSession;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startsession);
        btnStartSession=(Button)findViewById(R.id.btnStartSession);
        btnStartSession.setOnClickListener(this);
    }

    public void onClick(View v)
    {
        switch(v.getId()){

            case R.id.btnStartSession:
                Intent intent=new Intent(StartSession.this,HomeActivity.class);
                startActivity(intent);
                break;

        }

    }
}
