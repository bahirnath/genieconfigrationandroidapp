package gsmartadmin.genieiot.com.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import gsmartadmin.genieiot.com.myapplication.Utils.SwitchSessionManager;

/**
 * Created by root on 9/1/17.
 */

public class ChangePanelTopic extends AppCompatActivity implements View.OnClickListener{
    EditText edtChangeTopic;
    private Button btnSubmit;
     SwitchSessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_paneltopic_activity);
        initComponent();
    }

    private void initComponent() {
        edtChangeTopic= (EditText) findViewById(R.id.edtChangeTopic);
        btnSubmit= (Button) findViewById(R.id.btnSubmit);
        sessionManager=new SwitchSessionManager(this);

        edtChangeTopic.setText(sessionManager.getTopicName());
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:

                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                String mNewTopic=edtChangeTopic.getText().toString();
                if(mNewTopic.equals("")) {
                    Toast.makeText(ChangePanelTopic.this,"Please enter topic Name.",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!validate(mNewTopic)){
                    Toast.makeText(this, "Please enter IP in proper Format.", Toast.LENGTH_SHORT).show();
                    return;
                }

                sessionManager.setTopicName(mNewTopic);
                finish();
                Toast.makeText(this,"Update topic name successfully",Toast.LENGTH_SHORT).show();
                break;

        }


    }

    private static final String PATTERN =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    public static boolean validate(String ip){
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(ip);
        return matcher.matches();
    }
}
