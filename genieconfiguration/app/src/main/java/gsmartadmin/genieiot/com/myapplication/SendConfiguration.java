package gsmartadmin.genieiot.com.myapplication;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import gsmartadmin.genieiot.com.myapplication.Database.DatabaseHandler;
import gsmartadmin.genieiot.com.myapplication.Utils.SwitchSessionManager;
import gsmartadmin.genieiot.com.myapplication.Utils.Utils;

/**
 * Created by root on 23/1/17.
 */

public class SendConfiguration extends AppCompatActivity implements View.OnClickListener {

    Button btnSendConfiguration;
    private ArrayList<HashMap<String, String>> panelList;
    ProgressDialog progressDialog;
    SwitchSessionManager manager;
    private static final String TAG = "SendConfiguration";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sendconfiguration);
        initializeControls();
    }

    private void initializeControls() {
        btnSendConfiguration= (Button) findViewById(R.id.btnSendConfiguration);
        btnSendConfiguration.setOnClickListener(this);
        manager=new SwitchSessionManager(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
           case R.id.btnSendConfiguration:

             DatabaseHandler database = new DatabaseHandler(SendConfiguration.this);
             panelList = database.getAllHomeInfo();

             String mPanelList = "";
             String switchName="";

             //panel5-BD1-15-123456-FEcBF-L000-GR-1

             for (int pn = 0; pn < panelList.size(); pn++) {
             if (pn == panelList.size() - 1) {

                   mPanelList = mPanelList + panelList.get(pn).get(DatabaseHandler.TOPIC) + "-" + panelList.get(pn).get(DatabaseHandler.SWITCHDETAILS)+ "-" + panelList.get(pn).get(DatabaseHandler.CURTAINVALUE)+"-" + panelList.get(pn).get(DatabaseHandler.IOTPRODSHORTCUT)+"-"+panelList.get(pn).get(DatabaseHandler.LOCKVALUE);

             }
             else
             {
                 mPanelList = mPanelList + panelList.get(pn).get(DatabaseHandler.TOPIC) + "-" + panelList.get(pn).get(DatabaseHandler.SWITCHDETAILS) + "-" +panelList.get(pn).get(DatabaseHandler.CURTAINVALUE)+"-" + panelList.get(pn).get(DatabaseHandler.IOTPRODSHORTCUT)+ "-"+panelList.get(pn).get(DatabaseHandler.LOCKVALUE)+",";
              }

             }

                SwitchSessionManager session = new SwitchSessionManager(SendConfiguration.this);
                String mRouter = session.getPSSID() + "," + session.getPPASSWORD();
                String mSecure = session.getSSSID() + "," + session.getSPASSWORD();

                String mFinal = "";
                for (int pp = 0; pp < panelList.size(); pp++) {
                    mFinal = mFinal + panelList.get(pp).get(DatabaseHandler.SWITCHDETAILS) + "," + mRouter + "," + mSecure + "," + panelList.get(pp).get(DatabaseHandler.PANELDEATILS) + ",";
                    Log.d("ConfigDetails", mFinal);
                }

                Log.d("Panel List-->", mPanelList);



                if (panelList.size() > 0) {
                    new AsynConfigureFile().execute(mPanelList, mFinal);
                } else {
                    Toast.makeText(this, "Fill the Panel.", Toast.LENGTH_SHORT).show();
                }
        }

    }

    private class AsynConfigureFile extends AsyncTask<String,Void,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(SendConfiguration.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response="";
            Utils utils=new Utils();
            try {
                String jBody=createJSONBody(params[0],params[1]);
                Log.d("JSON Body",""+jBody);
                response=utils.doPostRequest(Utils.URL_GENIE+"/home/fillPanel",jBody+"");
                Log.d(TAG, "Fill Panel response: "+response);
            }
            catch(Exception e){}
            return response;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(progressDialog!=null && progressDialog.isShowing()){
                progressDialog.cancel();
            }

            if(result!=null)
            {
                Log.d("Home Details -->",result);
                try{
                    JSONObject jObj=new JSONObject(result);
                    if(jObj.getString("status").equals("SUCCESS"))
                    {
                        Toast.makeText(SendConfiguration.this, "Sync Config. file successfully.", Toast.LENGTH_SHORT).show();

                    }
                    else
                    {
                        Toast.makeText(SendConfiguration.this, "Fail to sync the Config. file.", Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){}
            }else{
                Toast.makeText(SendConfiguration.this, "Please try again.", Toast.LENGTH_SHORT).show();
            }
        }

    }


    private String createJSONBody(String strPanelList,String strIpConfig) {
        JSONObject jObj=new JSONObject();
        String USER_DETAILS=manager.getUSERDETAILS();
        DatabaseHandler db=new DatabaseHandler(SendConfiguration.this);
        String mTowerString=db.getGenieTower();

        try{

            JSONObject jUserDetailsObj=new JSONObject(USER_DETAILS);
            Log.d("All Data",""+jUserDetailsObj);

            jObj.put("panellist",strPanelList);
            jObj.put("ipConfigration",strIpConfig);
            jObj.put("towerInfo",mTowerString);

            jObj.put("id",jUserDetailsObj.getString("id"));
            jObj.put("lastName",jUserDetailsObj.getString("lastName"));
            jObj.put("phoneNumber",jUserDetailsObj.getString("phoneNumber"));
            jObj.put("email",jUserDetailsObj.getString("email"));
            jObj.put("deviceType",jUserDetailsObj.getString("deviceType"));
            jObj.put("image",jUserDetailsObj.getString("image"));
            jObj.put("firstName",jUserDetailsObj.getString("firstName"));
            jObj.put("password",jUserDetailsObj.getString("password"));
            jObj.put("deviceId",manager.getDeviceId());
            jObj.put("userType",jUserDetailsObj.getString("userType"));
            jObj.put("homeId",manager.getHOMEID());
            jObj.put("isEmailVerified",jUserDetailsObj.getString("isEmailVerified"));
            jObj.put("isFirstLogin",jUserDetailsObj.getString("isFirstLogin"));
            jObj.put("birthDate",jUserDetailsObj.getString("birthDate"));

            Log.d("JSON DATA",""+jObj);

        }catch (Exception e){}

        return jObj+"";

    }

}
