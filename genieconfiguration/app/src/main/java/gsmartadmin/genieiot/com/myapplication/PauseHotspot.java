package gsmartadmin.genieiot.com.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by root on 13/1/17.
 */

public class PauseHotspot extends AppCompatActivity implements View.OnClickListener {

    Button btnNextPanel;
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pause_hotspot_activity);
        btnNextPanel= (Button) findViewById(R.id.btnNextPanel);
        btnNextPanel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnNextPanel:
                finish();
                break;
        }
    }
}
